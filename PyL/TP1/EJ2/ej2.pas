{
  En una institución educativa se lleva registro del progreso académico de sus N
  alumnos. Uno de los datos importantes en este registro es el control de la asistencia.
  Para esto se utilizan dos listas aluDat ordenada por DNI y aluAsis ordenada por DNI y
  mes. En la primera lista se almacenan los datos personales del alumno: DNI, Nombre y
  Apellido, Edad, Curso, Domicilio, y Teléfono. En la segunda, las inasistencias por: DNI,
  mes, cantidad de Inasistencias.
  El Supervisor de cada turno debe entregar, al finalizar el día, la lista de inasistencias de
  los alumnos. Para esto entrega una lista llamada Asistencia con los siguientes Datos:
  DNI, mes, asistencia, donde el valor de asistencia es “P” o “A”. Esta lista no se
  encuentra ordenada ya que es cargada a medida que los alumnos ingresan al
  establecimiento. Un alumno puede quedar libre si suma 15 faltas.
  Se desea:

  a. Poder Cargar las Listas de datos personales e iniciar la lista de asistencia
     con valor a cero para cada alumno en el inicio de cada mes escolar.
  b. Cargar la lista Asistencia, entregada por el Supervisor.
  c. Actualizar la asistencia de los alumnos utilizando la lista ingresada por el
     Supervisor.
  d. Informar los alumnos que, luego de actualizar la asistencia, se encuentren
     libres.
}

PROGRAM TP1_EJ2;
USES
  CRT;

CONST
  MAX_STUDENTS        = 30; { Cantidad de estudiantes                       }
  MAX_MONTHS          = 12; { Cantidad de meses                             }
  MAX_STUDENT_ABSENT  = 15; { Cantidad de faltas para considerar libre a un
                              alumno                                        }

TYPE
  (* Estructura para la información de un estudiante *)
  TStudentData = Record
    id        : LongWord;   { número de documento/identificación  }
    name      : String;     { nombre completo                     }
    age       : Byte;       { edad                                }
    course    : String;     { curso                               }
    address   : String;     { dirección de residencia             }
    telephone : LongWord;   { número de teléfono                  }
  End;

  (* Estructura para la información de la asistencia mensual de un estudiante *)
  TStudentAttendance = Record
    id      : LongWord;     { número de documento/identificación  }
    month   : Byte;         { mes                                 }
    amount  : Byte;         { cantidad de inasistencias           }
  End;

  (* Enumeración de valores de asistencia posibles *)
  TAttendanceEnum =
  (
    A,  { ausente   }
    P   { presente  }
  );

  (* Estructura para el registro del supervisor de turno *)
  TAttendance = Record
    student_id  : LongWord;         { número de documento/identificación del
                                      alumno                                  }
    month       : Byte;             { mes del registro                        }
    attendance  : TAttendanceEnum;  { registro de asistencia; P/A             }
  End;

  (* Tipo de dato para la lista general de datos de los alumnos *)
  TStudentDataList        = Array[1..MAX_STUDENTS] of TStudentData;

  (* Tipo de dato para la lista de asistencias del supervisor de turno *)
  TDailyAttendanceList    = Array[1..MAX_STUDENTS] of TAttendance;

  (* Tipo de dato para la lista mensual de asistencias de los alumnos *)
  TMonthlyAttendanceList  = Array[1..MAX_STUDENTS] of TStudentAttendance;

  (* Tipo de dato para la lista general de asistencias de los alumnos *)
  TGeneralAttendanceList  = Array[1..12] of TMonthlyAttendanceList;


{//////////////////////////////////////////////////////////////////////////////}
(*  Inicializa una lista de datos de alumnos.
  *
  * @param list
  *   La lista que será inicializada.
  * @param n
  *   La cantidad de elementos en la lista.
*)
Procedure initializeStudentDataList(var list: TStudentDataList;
                                    n: Byte);
Var
  i: Byte; { para recorrer la lista de estudiantes }

Begin
  for i := 1 to n do
    begin
      with (list[i]) do
        begin
          id        := 0;
          name      := '';
          age       := 0;
          course    := '';
          address   := '';
          telephone := 0;
        end;
    end;
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Carga una lista de datos de alumnos.
  *
  * @param list
  *   La lista que será cargada.
  * @param n
  *   La cantidad de elementos de la lista.
*)
Procedure loadStudentDataList(var list: TStudentDataList;
                              n: Byte);
  procedure loadData(var data: TStudentData);
  begin
    with (data) do
      begin
        write('Ingrese el número de documento/identificación del alumno: ');
        readLn(id);

        write('Ingrese el nombre completo del alumno: ');
        readLn(name);

        write('Ingrese la edad de alumno: ');
        readLn(age);

        write('Ingrese el curso del alumno: ');
        readLn(course);

        write('Ingrese la dirección del alumno: ');
        readLn(address);

        write('Ingrese el número de teléfono del alumno: ');
        readLn(telephone);
      end;
  end;

  procedure insertSorted(var data: TStudentData;
                         var list: TStudentDataList;
                         n: Byte);
  var
    i: Byte;  { para recorrer la lista buscando la posicion }
    j: Byte;  { para realizar el reordenamiento             }

  begin
    i := 1;

    { busco la posición donde va a ser insertado }
    while ((i < n) and (list[i].id > data.id)) do
      inc(i);

    if (i <> n) then
      begin
        for j := n-1 downto i do
          list[j+1] := list[j];
      end;

    list[i] := data;
  end;

Var
  i   : Byte;         { para recorrer la lista        }
  aux : TStudentData; { para leer los datos a cargar  }

Begin
  for i := 1 to n do
    begin
      loadData(aux);
      insertSorted(aux, list, i);
    end;
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Inicializa una lista de asistencia diaria.
  *
  * @param month_id
  *   Identificador del mes.
  * @param list
  *   La lista que será inicializada.
  * @param n
  *   La cantidad de elementos de la lista.
*)
Procedure initializeDailyAttendanceList(month_id: Byte;
                                        var list: TDailyAttendanceList;
                                        n: Byte);
Var
  i : Byte; { para recorrer la lista }

Begin
  for i := 1 to n do
    begin
      with (list[i]) do
        begin
          student_id  := 0;
          month       := month_id;
          attendance  := A;
        end;
    end;
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Carga la lista diaria de asistencias.
  *
  * @param[out] list
  *   Lista donde se almacenaran los datos cargados.
  * @param[out] n
  *   Longitud de la lista una vez cargada.
*)
Procedure loadDailyList(var list: TDailyAttendanceList; var n: Byte);
  procedure loadAttendance(var attendance: TAttendance);
  var
    option: Char; { para leer la opcion del usuario }

  begin
    write('Ingrese el numero de documento/identificación del alumno: ');
    readLn(attendance.student_id);

    write('Se encuentra presente? (S/N): ');
    readLn(option);

    if (upcase(option) = 'S') then
      attendance.attendance := P
    else
      attendance.attendance := A;
  end;

Var
  option  : Char; { para la respuesta del usuario }

Begin
  write('Ingrese el mes de registro: ');
  readLn(n);

  initializeDailyAttendanceList(n, list, MAX_STUDENTS);

  n := 0;

  repeat
    clrScr();
    inc(n);

    loadAttendance(list[n]);

    write('Seguir tomando asistencia? (S/N)');
    readLn(option);

  until (upcase(option) = 'N');
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Inicializa una lista mensual de asistencias.
  *
  * @param month_id
  *   Identificador del mes.
  * @param list
  *   La lista que será inicializada.
  * @param students_data_list
  *   La lista con los datos de los estudiantes (para extraer DNI)
  * @param n
  *   La cantidad de elementos en la lista.
*)
Procedure initializeMonthlyAttendanceList(month_id: Byte;
                                          var list: TMonthlyAttendanceList;
                                          var students_data_list: TStudentDataList;
                                          n: Byte);
Var
  i : Byte; { para recorrer la lista }

Begin
  for i := 1 to n do
    begin
      with (list[i]) do
        begin
          id      := students_data_list[i].id;
          month   := month_id;
          amount  := 0;
        end;
    end;
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Inicializa la lista general de asistencias
  *
  * @param list
  *   La lista que será inicializada.
  * @param students_data_list
  *   La lista de datos de los estudiantes.
  * @param n
  *   La cantidad de elementos en la lista.
*)
Procedure initializeGeneralAttendanceList(var list: TGeneralAttendanceList;
                                          var students_data_list: TStudentDataList;
                                          n: Byte);
Var
  i : Byte; { para recorrer la lista general }

Begin
  for i := 1 to n do
    initializeMonthlyAttendanceList(i, list[i], students_data_list, MAX_STUDENTS);
End;

{//////////////////////////////////////////////////////////////////////////////}
(*  Mezcla una lista diaria con la lista general de asistencias.
  *
  * @param daily_list
  *   Lista diaria que será incluida en la general.
  * @param n_daily_list
  *   La cantidad de elementos en la lista diaria.
  * @param general_list
  *   Lista general donde se incluirán los cambios.
  * @param n_general_list
  *   Cantidad de meses en la lista general.
  * @param n_monthly_list
  *   La cantidad de elementos en la lista mensual (cada posición de la general)
*)
Procedure mergeStudentAttendanceList(var daily_list: TDailyAttendanceList;
                                     n_daily_list: Byte;
                                     var general_list: TGeneralAttendanceList;
                                     n_general_list: Byte;
                                     n_monthly_list: Byte);

  procedure reSortList(var list: TMonthlyAttendanceList; n: Byte);
  var
    i   : Byte;                 { para recorrer la lista  }
    j   : Byte;                 { para recorrer la lista  }
    k   : Byte;                 { para recorrer la lista  }
    aux : TStudentAttendance;   { para reordenar la lista }

  begin
    i := 1;

    while ((i < n) and (list[i].amount < list[i+1].amount)) do
      inc(i);

    if (i < n) then
      begin
        aux := list[i];

        j := i+1;

        while ((j < n) and (aux.amount > list[j].amount)) do
          inc(j);

        for k := i to j-1 do
          list[k] := list[k+1];

        list[j] := aux;
      end;
  end;

  procedure insertSorted(var attendance: TAttendance;
                         var list: TMonthlyAttendanceList;
                         n: Byte);
  var
    i : Byte; { para recorrer la lista }

  begin
    if (attendance.attendance = A) then
      begin
        i := 1;

        while (attendance.student_id <> list[i].id) do
          inc(i);

        inc(list[i].amount);

        reSortList(list, n);
      end;
  end;

Var
  i : Byte; { para recorrer la lista }

Begin
  for i := 1 to n_daily_list do
    insertSorted(daily_list[i], general_list[daily_list[i].month],
                 n_monthly_list);
End;


{//////////////////////////////////////////////////////////////////////////////}
(*  Muestra una lista de los alumnos libres.
  *
  * @param absent_to_free
  *   Cantidad da faltas para considerar libre a un alumno.
  * @param general_list
  *   Lista general de asistencias.
  * @param n_general_list
  *   Cantidad de meses de la lista general.
  * @param student_data_list
  *   Lista con los datos de los alumnos.
  * @param n_students
  *   Cantidad de alumnos.
*)
Procedure showFreeStudents(absent_to_free: Byte;
                           var general_list: TGeneralAttendanceList;
                           n_general_list: Byte;
                           var students_data_list: TStudentDataList;
                           n_students: Byte);

  procedure showStudentData(student_id: LongWord;
                            var list: TStudentDataList;
                            n: Byte
                           );
  var
    i : Byte = 1; { para recorrer la lista }

  begin
    while ((i <= n) and (student_id <> list[i].id)) do
      inc(i);

    if (i <= n) then
      with (list[i]) do
        begin
          writeLn('Nombre: ':15, name);
          writeLn('DNI/ID: ':15, id);
          writeLn('Edad: ':15, age);
          { mas datos a mostrar aquí }
        end;
  end;

  procedure showList(absent_to_free: Byte;
                     var free_students_list: TMonthlyAttendanceList;
                     var students_data_list: TStudentDataList;
                     n: Byte);
  var
    i : Byte; { para recorrer la lista }

  begin
    for i := 1 to n do
      begin
        if (free_students_list[i].amount >= absent_to_free) then
          begin
            showStudentData(free_students_list[i].id, students_data_list,
                            n);
            writeLn('Inasistencias: ': 15, free_students_list[i].amount);
            writeLn();
          end;
      end;
  end;

  function search(student_id: LongWord;
                  var list: TMonthlyAttendanceList;
                  n: Byte): Byte;
  var
    i : Byte = 1; { para recorrer la lista y ubicar la posición }

  begin
    search := 0;

    while ((i <= n) and (student_id <> list[i].id)) do
      inc(i);

    if (i <= n) then
      search := i;
  end;

  procedure composeList(var students_list: TMonthlyAttendanceList;
                        var monthly_list: TMonthlyAttendanceList;
                        n: Byte);
  var
    i   : Byte; { para recorrer la lista                    }
    pos : Byte; { para determinar la posicion a incrementar }

  begin
    for i := 1 to n do
      begin
        pos := search(monthly_list[i].id, students_list, n);

        if (pos <> 0) then
          inc(students_list[pos].amount, monthly_list[i].amount);
      end;
  end;

Var
  students_list : TMonthlyAttendanceList; { para la lista de estudiantes
                                              con las faltas acumuladas   }
  i             : Byte;                   { para recorrer la lista        }

Begin
  clrScr();

  initializeMonthlyAttendanceList(0, students_list, students_data_list,
                                  MAX_STUDENTS);
  for i := 1 to n_general_list do
    begin
      composeList(students_list, general_list[i], n_students);
    end;

  showList(absent_to_free, students_list, students_data_list, n_students);

  writeLn();
  writeLn('Presiona ENTER para continuar');
  readLn();
End;

(******************************************************************************)
(*********************************   PRINCIPAL    *****************************)
(******************************************************************************)

VAR
  option                  : Byte;                   { para la opcion del menú }
  sure                    : Char;                   { para la pregunta de
                                                      mezclar                 }
  student_data_list       : TStudentDataList;       { lista con los datos de
                                                      los estudiantes         }
  general_attendance_list : TGeneralAttendanceList; { lista general de
                                                      asistencia              }
  daily_attendance_list   : TDailyAttendanceList;   { lista de asistencia
                                                      diaria                  }
  n_daily                 : Byte = 0;               { cantidad de alumnos que
                                                      se tomaron lista        }

BEGIN
  initializeStudentDataList(student_data_list, MAX_STUDENTS);

  repeat
    clrScr();

    writeLn('1) Cargar lista de datos de alumnos.');
    writeLn('2) Cargar lista de asistencias.');
    writeLn('3) Integrar ultima lista de asistencias a la general.');
    writeLn('4) Listar los alumnos libres.');
    writeLn();
    writeLn('0) Salir');
    writeLn();
    write('# ');
    readLn(option);

    case (option) of
      1:
        begin
          loadStudentDataList(student_data_list, MAX_STUDENTS);

          initializeGeneralAttendanceList(general_attendance_list,
                                          student_data_list,
                                          MAX_STUDENTS);
        end;
      2:
        begin
          loadDailyList(daily_attendance_list, n_daily);
        end;

      3:
        begin
          mergeStudentAttendanceList(daily_attendance_list, n_daily,
                                     general_attendance_list, MAX_MONTHS,
                                     MAX_STUDENTS);
          n_daily := 0;
        end;

      4:
        begin
          if (n_daily > 0) then
            begin
              write('Hay una lista diaria aún no incluida, desea hacerlo ahora? (S/N): ');
              readLn(sure);
            end;

            if (upcase(sure) = 'S') then
              begin
                mergeStudentAttendanceList(daily_attendance_list, n_daily,
                                     general_attendance_list, MAX_MONTHS,
                                     MAX_STUDENTS);
                n_daily := 0;
              end;

          showFreeStudents(MAX_STUDENT_ABSENT,
                           general_attendance_list, MAX_MONTHS,
                           student_data_list, MAX_STUDENTS);
        end;
    end;
  until (option = 0);
END.
