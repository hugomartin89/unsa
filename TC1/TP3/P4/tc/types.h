/*
  Copyright (C) 2014 Hugo Martin <hugomartin89@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef TC_TYPES_H_INCLUDED
#define TC_TYPES_H_INCLUDED

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define TC_SINT8_MIN   ((tc_sint8_t)  0x80)
#define TC_SINT8_MAX   ((tc_sint8_t)  0x7f)
#define TC_UINT8_MIN   ((tc_uint8_t)  0x00)
#define TC_UINT8_MAX   ((tc_uint8_t)  0xff)
#define TC_SINT16_MIN  ((tc_sint16_t) 0x8000)
#define TC_SINT16_MAX  ((tc_sint16_t) 0x7fff)
#define TC_UINT16_MIN  ((tc_uint16_t) 0x0000)
#define TC_UINT16_MAX  ((tc_uint16_t) 0xffff)
#define TC_SINT32_MIN  ((tc_sint32_t) 0x80000000)
#define TC_SINT32_MAX  ((tc_sint32_t) 0x7fffffff)
#define TC_UINT32_MIN  ((tc_uint32_t) 0x00000000)
#define TC_UINT32_MAX  ((tc_uint32_t) 0xffffffff)
#define TC_SINT64_MIN  ((tc_sint64_t) 0x8000000000000000)
#define TC_SINT64_MAX  ((tc_sint64_t) 0x7fffffffffffffff)
#define TC_UINT64_MIN  ((tc_uint64_t) 0x0000000000000000)
#define TC_UINT64_MAX  ((tc_uint64_t) 0xffffffffffffffff)
#define TC_SIZE_MIN    ((tc_size_t)0)
#define TC_SIZE_MAX    ((tc_size_t)-1)
#define TC_FLOAT_MIN   FLT_MIN
#define TC_FLOAT_MAX   FLT_MAX
#define TC_DOUBLE_MIN  DBL_MIN
#define TC_DOUBLE_MAX  DBL_MAX
#define TC_NULL_PTR    ((tc_pointer_t)TC_SIZE_MIN)

typedef enum {false, true}  tc_bool_t;
typedef char                tc_char_t;
typedef tc_char_t*          tc_string_t;
typedef unsigned char       tc_uchar_t;
typedef signed char         tc_sint8_t;
typedef unsigned char       tc_uint8_t;
typedef signed short        tc_sint16_t;
typedef unsigned short      tc_uint16_t;
typedef signed int          tc_sint32_t;
typedef unsigned int        tc_uint32_t;
typedef signed long         tc_sint64_t;
typedef unsigned long       tc_uint64_t;
typedef size_t              tc_size_t;
typedef float               tc_float_t;
typedef double              tc_double_t;
typedef void*               tc_pointer_t;
typedef void* const         tc_constpointer_t;

#endif /* TC_TYPES_H_INCLUDED */
