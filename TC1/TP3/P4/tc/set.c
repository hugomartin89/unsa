/*
  Copyright (C) 2014 Hugo Martin <hugomartin89@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "set.h"
#include "assert.h"

typedef struct tc_set_node tc_set_node_t;

struct tc_set_node
{
  tc_pointer_t    data;
  tc_set_node_t*  left;
  tc_set_node_t*  right;
};

struct tc_set
{
  tc_size_t       element_size;
  tc_compare_fn   comparator;
  tc_destroy_fn   destructor;
  tc_set_node_t*  root;
};

/******************************************************************************/
tc_set_node_t* TC_SetNode_new(tc_constpointer_t data, tc_size_t data_size);
void TC_SetNode_free(tc_set_node_t* node, tc_destroy_fn destroy);
void TC_SetNode_freeAll(tc_set_node_t** node, tc_destroy_fn destroy);
tc_set_node_t** TC_SetNode_search(tc_set_node_t** node, tc_constpointer_t key, tc_compare_fn compare);
void TC_SetNode_insert(tc_set_node_t** node, tc_constpointer_t data, tc_compare_fn compare, tc_size_t data_size);
void TC_SetNode_delete(tc_set_node_t** node, tc_destroy_fn destroy);
tc_uint32_t TC_SetNode_count(tc_set_node_t* node);
void TC_SetNode_forEach(tc_set_node_t* node, tc_foreach_fn func, tc_pointer_t* user_data);

void TC_Set_makeUnionHelper(tc_constpointer_t data, tc_pointer_t* sets);
void TC_Set_makeIntersectionHelper(tc_constpointer_t data, tc_pointer_t* sets);
void TC_Set_makeDifferenceHelper(tc_constpointer_t data, tc_pointer_t* sets);

/******************************************************************************/
tc_set_t* TC_Set_new(tc_size_t element_size, tc_compare_fn comparator, tc_destroy_fn destructor)
{
  tc_set_t* self = TC_NULL_PTR;

  tc_assert(element_size > TC_SIZE_MIN,
    "Invalid \"element_size\" param");

  tc_assert(comparator != TC_NULL_PTR,
    "Invalid \"comparator\" param");

  tc_assert(destructor != TC_NULL_PTR,
    "Invalid \"destructor\" param");

  self = (tc_set_t*)tc_alloc(sizeof(tc_set_t));

  if (self != TC_NULL_PTR)
  {
    self->element_size  = element_size;
    self->comparator    = comparator;
    self->destructor    = destructor;
    self->root          = TC_NULL_PTR;
  }

  return self;
}

/******************************************************************************/
void TC_Set_free(tc_set_t* self)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  TC_Set_clear(self);

  tc_free(self);
}

/******************************************************************************/
void TC_Set_insert(tc_set_t* self, tc_constpointer_t data)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  tc_assert(data != TC_NULL_PTR,
    "Invalid \"data\" param");

  TC_SetNode_insert(&self->root, data, self->comparator, self->element_size);
}

/******************************************************************************/
void TC_Set_erase(tc_set_t* self, tc_constpointer_t data)
{
  tc_set_node_t** node = TC_NULL_PTR;

  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  tc_assert(data != TC_NULL_PTR,
    "Invalid \"data\" param");

  node = TC_SetNode_search(&self->root, data, self->comparator);

  if (*node != TC_NULL_PTR)
  {
    TC_SetNode_delete(node, self->destructor);
  }
}

/******************************************************************************/
void TC_Set_clear(tc_set_t* self)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  if (self->root != TC_NULL_PTR)
  {
    TC_SetNode_freeAll(&self->root, self->destructor);
  }
}

/******************************************************************************/
tc_bool_t TC_Set_empty(tc_set_t* self)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  return self->root == TC_NULL_PTR;
}

/******************************************************************************/
tc_uint32_t TC_Set_cardinal(tc_set_t* self)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  if (!TC_Set_empty(self))
  {
    return TC_SetNode_count(self->root);
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/
tc_bool_t TC_Set_belongs(tc_set_t* self, tc_constpointer_t data)
{
  tc_set_node_t** node = TC_NULL_PTR;

  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  tc_assert(data != TC_NULL_PTR,
    "Invalid \"key\" param");

  node = TC_SetNode_search(&self->root, data, self->comparator);

  return *node != TC_NULL_PTR;
}

/******************************************************************************/
tc_set_t* TC_Set_makeUnion(tc_set_t* set1, tc_set_t* set2)
{
  tc_set_t* result  = TC_NULL_PTR;

  tc_assert(set1 != TC_NULL_PTR,
    "Invalid \"set1\" param");

  tc_assert(set2 != TC_NULL_PTR,
    "Invalid \"set2\" param");

  tc_assert(set1->element_size == set2->element_size &&
    set1->comparator == set2->comparator &&
    set1->destructor == set2->destructor,
    "Mismatch sets");

  result = TC_Set_new(set1->element_size, set1->comparator, set1->destructor);

  TC_Set_forEach(set1, TC_Set_makeUnionHelper, (tc_pointer_t*)&result);
  TC_Set_forEach(set2, TC_Set_makeUnionHelper, (tc_pointer_t*)&result);

  return result;
}

/******************************************************************************/
tc_set_t* TC_Set_makeIntersection(tc_set_t* set1, tc_set_t* set2)
{
  tc_set_t* result  = TC_NULL_PTR;
  tc_set_t* sets[2] = {TC_NULL_PTR, TC_NULL_PTR};

  tc_assert(set1 != TC_NULL_PTR,
    "Invalid \"set1\" param");

  tc_assert(set2 != TC_NULL_PTR,
    "Invalid \"set2\" param");

  tc_assert(set1->element_size == set2->element_size &&
    set1->comparator == set2->comparator &&
    set1->destructor == set2->destructor,
    "Mismatch sets");

  result = TC_Set_new(set1->element_size, set1->comparator, set1->destructor);

  sets[0] = result;
  sets[1] = set2;

  TC_Set_forEach(set1, TC_Set_makeIntersectionHelper, (tc_pointer_t*)sets);

  return result;
}

/******************************************************************************/
tc_set_t* TC_Set_makeDifference(tc_set_t* set1, tc_set_t* set2)
{
  tc_set_t* result  = TC_NULL_PTR;
  tc_set_t* sets[2] = {TC_NULL_PTR, TC_NULL_PTR};

  tc_assert(set1 != TC_NULL_PTR,
    "Invalid \"set1\" param");

  tc_assert(set2 != TC_NULL_PTR,
    "Invalid \"set2\" param");

  tc_assert(set1->element_size == set2->element_size &&
    set1->comparator == set2->comparator &&
    set1->destructor == set2->destructor,
    "Mismatch sets");

  result = TC_Set_new(set1->element_size, set1->comparator, set1->destructor);

  sets[0] = result;
  sets[1] = set2;

  TC_Set_forEach(set1, TC_Set_makeDifferenceHelper, (tc_pointer_t*)sets);

  return result;
}

/******************************************************************************/
void TC_Set_forEach(tc_set_t* self, tc_foreach_fn func, tc_pointer_t* user_data)
{
  tc_assert(self != TC_NULL_PTR,
    "Invalid \"self\" param");

  tc_assert(func != TC_NULL_PTR,
    "Invalid \"func\" param");

  TC_SetNode_forEach(self->root, func, user_data);
}

/******************************************************************************/
tc_set_node_t* TC_SetNode_new(tc_constpointer_t data, tc_size_t data_size)
{
  tc_set_node_t* node = TC_NULL_PTR;

  node = (tc_set_node_t*)tc_alloc(sizeof(tc_set_node_t));

  if (node != TC_NULL_PTR)
  {
    node->data  = (tc_pointer_t)tc_alloc(data_size);
    node->left  = TC_NULL_PTR;
    node->right = TC_NULL_PTR;

    tc_copy(node->data, data, data_size);
  }

  return node;
}

/******************************************************************************/
void TC_SetNode_free(tc_set_node_t* node, tc_destroy_fn destroy)
{
  destroy(node->data);

  tc_free(node);
}

/******************************************************************************/
void TC_SetNode_freeAll(tc_set_node_t** node, tc_destroy_fn destroy)
{
  if (*node != TC_NULL_PTR)
  {
    TC_SetNode_freeAll(&(*node)->left, destroy);
    TC_SetNode_freeAll(&(*node)->left, destroy);

    destroy((*node)->data);

    tc_free(*node);

    *node = TC_NULL_PTR;
  }
}

/******************************************************************************/
tc_set_node_t** TC_SetNode_search(tc_set_node_t** node, tc_constpointer_t key, tc_compare_fn compare)
{
  tc_set_node_t** tmp   = node;
  tc_sint8_t      comp  = 0;

  do
  {
    if (*tmp != TC_NULL_PTR)
    {
      comp = compare(key, (*tmp)->data);
    }

    if (comp < 0)
    {
      tmp = &(*tmp)->left;
    }
    else if (comp > 0)
    {
      tmp = &(*tmp)->right;
    }
  }
  while (*tmp != TC_NULL_PTR && comp != 0);

  return tmp;
}

/******************************************************************************/
void TC_SetNode_insert(tc_set_node_t** node, tc_constpointer_t data, tc_compare_fn compare, tc_size_t data_size)
{
  tc_set_node_t** tmp = TC_NULL_PTR;

  tmp = TC_SetNode_search(node, data, compare);

  if (*tmp == TC_NULL_PTR)
  {
    *tmp = TC_SetNode_new(data, data_size);
  }
}

/******************************************************************************/
void TC_SetNode_delete(tc_set_node_t** node, tc_destroy_fn destroy)
{
  tc_set_node_t*  tmp1  = TC_NULL_PTR;
  tc_pointer_t    data  = TC_NULL_PTR;

  tmp1 = *node;

  if ((*node)->left == TC_NULL_PTR)
  {
    *node = (*node)->right;

    TC_SetNode_free(tmp1, destroy);
  }
  else if ((*node)->right == TC_NULL_PTR)
  {
    *node = (*node)->left;

    TC_SetNode_free(tmp1, destroy);
  }
  else
  {
    tmp1 = (*node)->left;

    while (tmp1->right != TC_NULL_PTR)
    {
      tmp1 = tmp1->right;
    }

    data          = tmp1->data;
    tmp1->data    = (*node)->data;
    (*node)->data = data;

    TC_SetNode_delete(&tmp1, destroy);
  }
}

/******************************************************************************/
tc_uint32_t TC_SetNode_count(tc_set_node_t* node)
{
  if (node != TC_NULL_PTR)
  {
    return 1 + TC_SetNode_count(node->left) + TC_SetNode_count(node->right);
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/
void TC_SetNode_forEach(tc_set_node_t* node, tc_foreach_fn func, tc_pointer_t* user_data)
{
  if (node != TC_NULL_PTR)
  {
    TC_SetNode_forEach(node->left, func, user_data);
    func(node->data, user_data);
    TC_SetNode_forEach(node->right, func, user_data);
  }
}

/******************************************************************************/
void TC_Set_makeUnionHelper(tc_constpointer_t data, tc_pointer_t* sets)
{
  tc_set_t* result  = (tc_set_t*)sets[0];

  TC_Set_insert(result, data);
}

/******************************************************************************/
void TC_Set_makeIntersectionHelper(tc_constpointer_t data, tc_pointer_t* sets)
{
  tc_set_t* result  = (tc_set_t*)sets[0];
  tc_set_t* other   = (tc_set_t*)sets[1];

  if (TC_Set_belongs(other, data))
  {
    TC_Set_insert(result, data);
  }
}

/******************************************************************************/
void TC_Set_makeDifferenceHelper(tc_constpointer_t data, tc_pointer_t* sets)
{
  tc_set_t* result  = (tc_set_t*)sets[0];
  tc_set_t* other   = (tc_set_t*)sets[1];

  if (!TC_Set_belongs(other, data))
  {
    TC_Set_insert(result, data);
  }
}
