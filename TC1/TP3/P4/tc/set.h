/*
  Copyright (C) 2014 Hugo Martin <hugomartin89@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef TC_SET_H_INCLUDED
#define TC_SET_H_INCLUDED

#include "types.h"
#include "prototypes.h"

typedef struct tc_set tc_set_t;

tc_set_t* TC_Set_new(tc_size_t element_size, tc_compare_fn comparator, tc_destroy_fn destructor);
void TC_Set_free(tc_set_t* self);
void TC_Set_insert(tc_set_t* self, tc_constpointer_t data);
void TC_Set_erase(tc_set_t* self, tc_constpointer_t data);
void TC_Set_clear(tc_set_t* self);
tc_bool_t TC_Set_empty(tc_set_t* self);
tc_uint32_t TC_Set_cardinal(tc_set_t* self);
tc_bool_t TC_Set_belongs(tc_set_t* self, tc_constpointer_t data);
tc_set_t* TC_Set_makeUnion(tc_set_t* set1, tc_set_t* set2);
tc_set_t* TC_Set_makeIntersection(tc_set_t* set1, tc_set_t* set2);
tc_set_t* TC_Set_makeDifference(tc_set_t* set1, tc_set_t* set2);
void TC_Set_forEach(tc_set_t* self, tc_foreach_fn func, tc_pointer_t* user_data);

#endif /* TC_SET_H_INCLUDED */
