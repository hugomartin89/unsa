/*
  Copyright (C) 2014 Hugo Martin <hugomartin89@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "memory.h"
#include "assert.h"

#include <stdlib.h>

/******************************************************************************/
tc_pointer_t tc_alloc(tc_size_t size)
{
  tc_assert(size > TC_SIZE_MIN,
    "Invalid \"size\" param");

#ifdef TC_MEMORY_CALLOC
  return (tc_pointer_t)calloc((size_t)size);
#else
  return (tc_pointer_t)malloc((size_t)size);
#endif /* TC_MEMORY_CALLOC */
}

/******************************************************************************/
tc_pointer_t tc_realloc(tc_pointer_t pointer, tc_size_t size)
{
  return (tc_pointer_t)realloc((void*)pointer, (size_t)size);
}

/******************************************************************************/
tc_pointer_t tc_copy(tc_pointer_t destination, tc_constpointer_t source, tc_size_t size)
{
  tc_assert(destination != TC_NULL_PTR,
    "Invalid \"destination\" param");

  tc_assert(source != TC_NULL_PTR,
    "Invalid \"source\" param");

  tc_assert(size > TC_SIZE_MIN,
    "Invalid \"size\" param");

  return (tc_pointer_t)memcpy((void*)destination, (void*)source, (size_t)size);
}

/******************************************************************************/
void tc_free(tc_pointer_t pointer)
{
  tc_assert(pointer != TC_NULL_PTR,
    "Invalid \"pointer\" param");

  free(pointer);
}
