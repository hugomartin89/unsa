UNIT UHuffmanTree;
INTERFACE
USES
  SysUtils,
  UFrequencyList,
  UHuffmanCodeList,
  UUtils;

TYPE
  PHuffmanTreeNode = ^THuffmanTreeNode;
  THuffmanTreeNode = Record
    code      : Integer;          { codigo del nodo                           }
    frequency : LongWord;         { frecuencia de aparicion del codigo        }
    left      : PHuffmanTreeNode; { hijo izquierdo del nodo                   }
    right     : PHuffmanTreeNode; { hijo derecho del nodo                     }
    next      : PHuffmanTreeNode; { puntero al elemento siguiente en la lista }
  End;

  PHuffmanTree = PHuffmanTreeNode;

Function HuffmanTree_newFromFrequencyList(var list: PFrequencyList): PHuffmanTree;
Function HuffmanTree_newFromStringSpec(var str: AnsiString): PHuffmanTree;
Procedure HuffmanTree_free(var tree: PHuffmanTree);
Procedure HuffmanTree_clear(var tree: PHuffmanTree);
Function HuffmanTree_getCodeList(var tree: PHuffmanTree): PHuffmanCodeList;
Function HuffmanTree_getTreeSpec(tree: PHuffmanTree): AnsiString;
Function HuffmanTree_getOriginalMessage(var tree: PHuffmanTree; var str: AnsiString): AnsiString;

IMPLEMENTATION

Procedure HuffmanTree_compute(var tree: PHuffmanTree); Forward;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_newFromFrequencyList(var list: PFrequencyList): PHuffmanTree;
Var
  code  : Integer;
  node  : PHuffmanTreeNode;
  aux   : PHuffmanTreeNode;

Begin
  HuffmanTree_newFromFrequencyList := NIL;

  if (assigned(list)) then
    begin
      code  := FrequencyList_getCodeMinFrequency(list);
      aux   := HuffmanTree_newFromFrequencyList;

      while (code > -1) do
        begin
          new(node);
          node^.code      := code;
          node^.frequency := FrequencyList_get(list, code);
          node^.left      := NIL;
          node^.right     := NIL;
          node^.next      := NIL;

          if (aux = NIL) then
            begin
              HuffmanTree_newFromFrequencyList  := node;
              aux                               := node;
            end
          else
            begin
              aux^.next := node;
              aux       := aux^.next;
            end;

          node := NIL;

          FrequencyList_set(list, code, 0);
          code := FrequencyList_getCodeMinFrequency(list);
        end;

      HuffmanTree_compute(HuffmanTree_newFromFrequencyList);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_newFromStringSpec(var str: AnsiString): PHuffmanTree;
  procedure buildTree(var node: PHuffmanTree; var str: AnsiString; var pos: QWord);
  begin
    if (pos <= length(str)) then
      begin
        if (str[pos] = '0') then
          begin
            new(node);
            node^.code      := -1;
            node^.frequency := 0;
            node^.left      := NIL;
            node^.right     := NIL;
            node^.next      := NIL;

            inc(pos);
            buildTree(node^.left, str, pos);
            buildTree(node^.right, str, pos);
          end
        else
          begin
            inc(pos);
            new(node);
            node^.code      := binStrToDec(copy(str, pos, 8));
            node^.frequency := 0;
            node^.left      := NIL;
            node^.right     := NIL;
            node^.next      := NIL;
            inc(pos, 8);
          end;
      end;
  end;

Var
  pos: QWord;

Begin
  HuffmanTree_newFromStringSpec := NIL;
  pos := 1;
  buildTree(HuffmanTree_newFromStringSpec, str, pos);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_free(var tree: PHuffmanTree);
Begin
  if (assigned(tree)) then
    begin
      HuffmanTree_clear(tree);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_clear(var tree: PHuffmanTree);
  procedure clearNode(var node: PHuffmanTreeNode);
  begin
    if (node <> NIL) then
      begin
        clearNode(node^.left);
        clearNode(node^.right);
        dispose(node);
        node := NIL;
      end;
  end;

Var
  node  : PHuffmanTreeNode; { nodo auxiliar para eliminar }

Begin
  if (assigned(tree)) then
    begin
      while (tree <> NIL) do
        begin
          node  := tree;
          tree  := tree^.next;
          clearNode(node);

          dispose(node);
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_getCodeList(var tree: PHuffmanTree): PHuffmanCodeList;
  procedure getCode(var node: PHuffmanTreeNode; code: Byte; value: String;
                    right: Boolean; var list: PHuffmanCodeList);
  begin
    if (node <> NIL) then
      begin
        if (right) then
          value := value + '1'
        else
          value := value + '0';

        getCode(node^.left, node^.code, value, FALSE, list);
        getCode(node^.right, node^.code, value, TRUE, list);
      end
    else
      HuffmanCodeList_set(list, code, value)
  end;

Begin
  HuffmanTree_getCodeList := NIL;

  if (assigned(tree)) then
    begin
      HuffmanTree_getCodeList := HuffmanCodeList_new();

      if (assigned(tree)) then
        begin
          if (assigned(tree^.left)) then
            getCode(tree^.left, tree^.left^.code, '', FALSE,
                    HuffmanTree_getCodeList
                   );

          if (assigned(tree^.right)) then
            getCode(tree^.right, tree^.right^.code, '', TRUE,
                    HuffmanTree_getCodeList
                   );
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanTree_compute(var tree: PHuffmanTree);
Var
  aux       : PHuffmanTreeNode; { nodo auxiliar para insertar                 }
  pos       : PHuffmanTreeNode; { nodo para recorrer el arbol                 }
  prev      : PHuffmanTreeNode; { nodo previo al de recorrido para reconectar }

Begin
  if (assigned(tree)) then
    begin
      while (tree^.next <> NIL) do
        begin
          new(aux);

          aux^.code       := -1;
          aux^.frequency  := tree^.frequency;
          aux^.right      := tree;

          tree := tree^.next;
          inc(aux^.frequency, tree^.frequency);
          aux^.left := tree;

          aux^.next       := NIL;

          tree := tree^.next;

          aux^.left^.next   := NIL;
          aux^.right^.next  := NIL;

          prev  := NIL;
          pos   := tree;

          while ((pos <> NIL) and (aux^.frequency >= pos^.frequency)) do
            begin
              prev  := pos;
              pos   := pos^.next;
            end;

          if (prev = NIL) then
            begin
              aux^.next := tree;
              tree      := aux;
            end
          else
            begin
              aux^.next   := pos;
              prev^.next  := aux;
            end;
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_getTreeSpec(tree: PHuffmanTree): AnsiString;
  procedure getCode(tree: PHuffmanTree; var code: AnsiString);
  var
    aux: AnsiString;

  begin
    if (tree <> NIL) then
      begin
        if ((tree^.left <> NIL) or (tree^.right <> NIL)) then
          begin
            code := code + '0';
            getCode(tree^.left, code);
            getCode(tree^.right, code);
          end
        else
          begin
            aux := decToBinStr(tree^.code);
            completeStrByte(aux);
            code := code + '1' + aux;
          end;
      end;
  end;

Begin
  HuffmanTree_getTreeSpec := '';

  if (assigned(tree)) then
    begin
      getCode(tree, HuffmanTree_getTreeSpec);
    end
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanTree_getOriginalMessage(var tree: PHuffmanTree; var str: AnsiString): AnsiString;
  function compose(node: PHuffmanTree; var str: AnsiString; var pos: QWord): AnsiString;
  begin
    if (node <> NIL) then
      begin
        if ((node^.left = NIL) and (node^.right = NIL)) then
          begin
            compose := decToBinStr(node^.code);
            completeStrByte(compose);
          end
        else
          begin
            if (str[pos] = '0') then
              begin
                inc(pos);
                compose := compose(node^.left, str, pos);
              end
            else
              begin
                inc(pos);
                compose := compose(node^.right, str, pos);
              end;
          end;
      end;
  end;

Var
  len : QWord;
  pos : QWord;
Begin
  HuffmanTree_getOriginalMessage := '';

  len := length(str);
  if (len > 0) then
    begin
      pos := 1;
      while (pos <= len) do
        HuffmanTree_getOriginalMessage := HuffmanTree_getOriginalMessage +
                                          compose(tree, str, pos);
      end;
End;

END.
