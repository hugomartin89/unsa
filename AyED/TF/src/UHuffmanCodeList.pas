UNIT UHuffmanCodeList;
INTERFACE
TYPE
  PHuffmanCodeList = ^THuffmanCodeList;
  THuffmanCodeList = Array [0..255] of AnsiString;

Function HuffmanCodeList_new(): PHuffmanCodeList;
Procedure HuffmanCodeList_free(var list: PHuffmanCodeList);
Procedure HuffmanCodeList_clear(var list: PHuffmanCodeList);
Procedure HuffmanCodeList_set(var list: PHuffmanCodeList; code: Byte; value: AnsiString);
Function HuffmanCodeList_get(var list: PHuffmanCodeList; code: Byte): AnsiString;


IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function HuffmanCodeList_new(): PHuffmanCodeList;
Begin
  new(HuffmanCodeList_new);

  HuffmanCodeList_clear(HuffmanCodeList_new);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanCodeList_free(var list: PHuffmanCodeList);
Begin
  if (assigned(list)) then
    begin
      dispose(list);

      list := NIL;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanCodeList_clear(var list: PHuffmanCodeList);
Var
  i: Byte;

Begin
  if (assigned(list)) then
    begin
      for i := 0 to 255 do
        list^[i] := '';
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure HuffmanCodeList_set(var list: PHuffmanCodeList; code: Byte; value: AnsiString);
Begin
  if (assigned(list)) then
    list^[code] := value;
End;

////////////////////////////////////////////////////////////////////////////////
Function HuffmanCodeList_get(var list: PHuffmanCodeList; code: Byte): AnsiString;
Begin
  if (assigned(list)) then
    HuffmanCodeList_get := list^[code];
End;

END.
