UNIT UUtils;
INTERFACE

Function binStrToDec(bin: AnsiString): Byte;
Function decToBinStr(dec: QWord): AnsiString;
Function completeStrByte(var str: AnsiString): Byte;
Function extractByteStr(var str: AnsiString): AnsiString;

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function binStrToDec(bin: AnsiString): Byte;
Var
  i: Byte;

Begin
  if (length(bin) > 0) then
    begin
      binStrToDec := 0;

      for i := 1 to length(bin) do
        begin
          binStrToDec := binStrToDec * 2;

          if (bin[i] = '1') then
            inc(binStrToDec);
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function decToBinStr(dec: QWord): AnsiString;
Begin
  if (dec < 2) then
    begin
      if (dec = 0) then
        decToBinStr := '0'
      else
        decToBinStr := '1';
    end
  else
    if (dec mod 2 = 0) then
      decToBinStr := decToBinStr(dec div 2) + '0'
    else
      decToBinStr := decToBinStr(dec div 2) + '1';
End;

////////////////////////////////////////////////////////////////////////////////
Function completeStrByte(var str: AnsiString): Byte;
Begin
  completeStrByte := 0;

  while ((length(str) mod 8) <> 0) do
    begin
      str := '0' + str;
      inc(completeStrByte);
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function extractByteStr(var str: AnsiString): AnsiString;
Begin
  if (length(str) < 8) then
    begin
      extractByteStr  := str;
      str             := '';
    end
  else
    begin
      extractByteStr := copy(str, 1, 8);
      delete(str, 1, 8);
    end;
End;

END.
