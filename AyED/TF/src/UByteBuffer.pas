{
  * TAD para manejar un "buffer" de "byte"
}
UNIT UByteBuffer;
INTERFACE
USES
  UUtils;

TYPE
  PByteBuffer = ^TByteBuffer;
  TByteBuffer = Record
    data    : PByte;  { datos del buffer    }
    length  : QWord;  { longitud del buffer }
  End;


Function ByteBuffer_new(var data: PByte; length: QWord): PByteBuffer;
Function ByteBuffer_newFromBinaryStr(var str: AnsiString): PByteBuffer;
Procedure ByteBuffer_free(var buffer: PByteBuffer);
Function ByteBuffer_data(var buffer: PByteBuffer): PByte;
Function ByteBuffer_length(var buffer: PByteBuffer): QWord;
Function ByteBuffer_get(var buffer: PByteBuffer; pos: QWord): Byte;

IMPLEMENTATION
////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_new(var data: PByte; length: QWord): PByteBuffer;
Begin
  new(ByteBuffer_new);

  ByteBuffer_new^.data    := data;
  ByteBuffer_new^.length  := length;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_newFromBinaryStr(var str: AnsiString): PByteBuffer;
Var
  pos     : QWord;
  len     : QWord;
  i       : QWord;
  code    : AnsiString;
  number  : Byte;

Begin
  new(ByteBuffer_newFromBinaryStr);
  ByteBuffer_newFromBinaryStr^.data   := NIL;
  ByteBuffer_newFromBinaryStr^.length := 0;
  len                                 := length(str);

  if (len > 0) then
    begin
      ByteBuffer_newFromBinaryStr^.length := length(str) div 8;
      getMem(ByteBuffer_newFromBinaryStr^.data,
             ByteBuffer_newFromBinaryStr^.length
            );

      pos := 1;
      i   := 0;
      while (pos < len) do
        begin
          code    := copy(str, pos, 8);
          number  := binStrToDec(code);
          ByteBuffer_newFromBinaryStr^.data[i] := number;
          inc(pos, 8);
          inc(i);
        end;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Procedure ByteBuffer_free(var buffer: PByteBuffer);
Begin
  if (assigned(buffer)) then
    begin
      freeMem(buffer^.data);
      buffer^.length := 0;

      dispose(buffer);

      buffer := NIL;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_data(var buffer: PByteBuffer): PByte;
Begin
  ByteBuffer_data := NIL;

  if (assigned(buffer)) then
    begin
      ByteBuffer_data := buffer^.data;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_length(var buffer: PByteBuffer): QWord;
Begin
  ByteBuffer_length := 0;

  if (assigned(buffer)) then
    begin
      ByteBuffer_length := buffer^.length;
    end;
End;

////////////////////////////////////////////////////////////////////////////////
Function ByteBuffer_get(var buffer: PByteBuffer; pos: QWord): Byte;
Begin
  ByteBuffer_get := 0;

  if (assigned(buffer)) then
    begin
      if (pos <= buffer^.length) then
        ByteBuffer_get := buffer^.data[pos - 1];
    end;
End;

END.
