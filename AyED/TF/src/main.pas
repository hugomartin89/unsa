unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, UFile, UDBZ;

type

  { TMainWindow }

  TMainWindow = class(TForm)
    FMainMenu: TMainMenu;
    FMenuFile: TMenuItem;
    FOpenDialog: TOpenDialog;
    FMenuAction: TMenuItem;
    FActionCompress: TMenuItem;
    FActionDecompress: TMenuItem;
    FFileNameLabel: TLabel;
    FFileSizeLabel: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    FSaveDialog: TSaveDialog;
    FFileOpen: TMenuItem;
    procedure FActionCompressClick(Sender: TObject);
    procedure FActionDecompressClick(Sender: TObject);
    procedure FFileOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FSaveFileName: AnsiString;
  public
    { public declarations }
  end;

var
  MainWindow: TMainWindow;

implementation

{$R *.lfm}

{ TMainWindow }

procedure TMainWindow.FormCreate(Sender: TObject);
begin
  Height := 125;
end;

procedure TMainWindow.FFileOpenClick(Sender: TObject);
var
  f      : PFile;
  unity  : Byte;
  size   : Single;

begin
  if (FOpenDialog.Execute()) then
    begin
      f := File_open(FOpenDialog.FileName, FILE_MODE_READ);
      Label1.Caption := File_name(f);

      size  := File_size(f);
      unity := 0;

      while (size > 1024.0) do
        begin
          inc(unity);
          size := size / 1024;
        end;

      Label2.Caption := floatToStr(size);
      Label2.Caption := copy(Label2.Caption, 1, 5);
      File_close(f);

      case (unity) of
        0 : Label2.Caption := Label2.Caption + ' Bytes';
        1 : Label2.Caption := Label2.Caption + ' Kb';
        2 : Label2.Caption := Label2.Caption + ' Mb';
        3 : Label2.Caption := Label2.Caption + ' Gb';
      end;

      FSaveFileName := FOpenDialog.FileName;

      if (pos('.DBZ', upcase(Label1.Caption)) <> 0) then
        begin
          FActionDecompress.Enabled := TRUE;
          FActionCompress.Enabled   := FALSE;
        end
      else
        begin
          FActionDecompress.Enabled := FALSE;
          FActionCompress.Enabled   := TRUE;
        end;
    end;
end;

procedure TMainWindow.FActionCompressClick(Sender: TObject);
begin
  FSaveDialog.FileName := '';
  FSaveDialog.Filter := 'Archivo DBZ|*.dbz;*.Dbz;*.dBz;*.dbZ;*.DBz;*.DbZ;*.DBZ';
  if (FSaveDialog.Execute()) then
    begin
      {$IFNDEF MSWINDOWS}
      if (pos('.DBZ', upcase(FSaveDialog.FileName)) = 0) then
        FSaveDialog.FileName := FSaveDialog.FileName + '.dbz';
      {$ENDIF}
      DBZ_compress(FSaveFileName, FSaveDialog.FileName);
      ShowMessage('Archivo ' + FSaveFileName + ' comprimido correctamente en ' + FSaveDialog.FileName);
    end;
end;

procedure TMainWindow.FActionDecompressClick(Sender: TObject);
begin
  FSaveDialog.Filter := '';

  if (FSaveDialog.Execute()) then
    begin
      DBZ_decompress(FSaveFileName, FSaveDialog.FileName);
      ShowMessage('Archivo ' + FSaveFileName + ' descomprimido correctamente.');
    end;
end;

end.

