UNIT UDBZ;
INTERFACE
USES
  UFile,
  UByteBuffer,
  UFrequencyList,
  UHuffmanTree,
  UHuffmanCodeList,
  UUtils,
  SysUtils;

Procedure DBZ_compress(file_in, file_out: AnsiString);
Procedure DBZ_decompress(file_in, file_out: AnsiString);

IMPLEMENTATION

Procedure writeFile(var f: PFile; var str: AnsiString); Forward;

////////////////////////////////////////////////////////////////////////////////
Procedure DBZ_compress(file_in, file_out: AnsiString);
Const
  READ_BUFFER_SIZE = 256;

Var
  handle_in   : PFile;                { archivo de entrada                    }
  handle_out  : PFile;                { archivo de salida                     }
  freq_list   : PFrequencyList;       { lista de frecuencias                  }
  code_list   : PHuffmanCodeList;     { lista de codigos del arbol            }
  buffer      : PByteBuffer;          { buffer de lectura                     }
  i           : 1..READ_BUFFER_SIZE;  { para recorrer el buffer de lectura    }
  tree        : PHuffmanTree;         { arbol de compresión                   }
  msj         : AnsiString;           { almacena el mansaje codificado        }
  tree_spec   : AnsiString;           { almacena la especificacion del arbol  }
  header      : AnsiString;           { para generar la cabecera              }
  aux         : AnsiString;           { cadena para formar la cabecera        }
  trunk_tree  : Byte;                 { tamaño de truncado del arbol          }
  trunk_msj   : Byte;                 { tamaño de truncado del mensaje        }

Begin
  handle_in   := File_open(file_in, FILE_MODE_READ);
  handle_out  := File_open(file_out, FILE_MODE_WRITE);

  if (assigned(handle_in)) then
    begin
      freq_list := FrequencyList_new();

      { leo el archivo de entrada }
      repeat
        buffer := File_read(handle_in, READ_BUFFER_SIZE);

        for i := 1 to ByteBuffer_length(buffer) do
          FrequencyList_inc(freq_list, ByteBuffer_get(buffer, i));

        ByteBuffer_free(buffer);
      until (File_tell(handle_in) = File_size(handle_in));

      { genero el arbol de compresion }
      tree := HuffmanTree_newFromFrequencyList(freq_list);

      { obtengo la lista de codigos de bytes a partir del arbol }
      code_list := HuffmanTree_getCodeList(tree);

      { genero el mensaje codificado }
      msj := '';

      File_seek(handle_in, 0);
      repeat
        buffer := File_read(handle_in, 1);
        msj := msj + HuffmanCodeList_get(code_list, ByteBuffer_get(buffer, 1));
        ByteBuffer_free(buffer);
      until (File_tell(handle_in) = File_size(handle_in));

      { obtengo la especificacion del arbol }
      tree_spec := HuffmanTree_getTreeSpec(tree);

      { redondeo el mensaje y la especificacion del arbol }
      trunk_tree  := completeStrByte(tree_spec);
      trunk_msj   := completeStrByte(msj);

      { armo la cabecera }
      header := '';

      { DBZ para determinar el tipo de archivo }
      aux := decToBinStr(ord('D'));
      completeStrByte(aux);
      header := header + aux;

      aux := decToBinStr(ord('B'));
      completeStrByte(aux);
      header := header + aux;

      aux := decToBinStr(ord('Z'));
      completeStrByte(aux);
      header := header + aux;

      { longitud de la extension }
      aux := decToBinStr(length(File_extension(handle_in)));
      completeStrByte(aux);
      header := header + aux;

      { longitud del arbol }
      aux := decToBinStr(length(tree_spec) div 8);

      while ((length(aux) div 8) < 2) do
        aux := '0' + aux;
      header := header + aux;

      { truncamiento del arbol }
      aux     := decToBinStr(trunk_tree);
      completeStrByte(aux);
      header  := header + aux;

      { longitud del mensaje }
      aux := decToBinStr(length(msj) div 8);

      while ((length(aux) div 8) < 4) do
        aux := '0' + aux;

      header  := header + aux;

      { truncamiento del mensaje }
      aux     := decToBinStr(trunk_msj);
      completeStrByte(aux);
      header  := header + aux;

      { extension }
      for i := 1 to length(File_extension(handle_in)) do
        begin
          aux := decToBinStr(ord(File_extension(handle_in)[i]));
          completeStrByte(aux);
          header := header + aux;
        end;

      { escribo el archivo }
      writeFile(handle_out, header);
      header := '';

      { escribo el cuerpo }
      { escribo el arbol }
      writeFile(handle_out, tree_spec);
      tree_spec := '';

      { escribo el mansaje }
      writeFile(handle_out, msj);
      msj := '';

      { libero los recursos usados }
      FrequencyList_free(freq_list);
      HuffmanCodeList_free(code_list);
      HuffmanTree_free(tree);
    end;

  if (File_opened(handle_in)) then
    File_close(handle_in);

  if (File_opened(handle_out)) then
    File_close(handle_out);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure DBZ_decompress(file_in, file_out: AnsiString);
Var
  handle_in       : PFile;
  handle_out      : PFile;
  buffer          : PByteBuffer;
  ext_size        : Byte;
  i               : QWord;
  tree_spec_size  : LongWord;
  trunk_tree      : Byte;
  msj_size        : QWord;
  trunk_msj       : Byte;
  aux             : AnsiString;
  tree_spec       : AnsiString;
  msj             : AnsiString;
  tree            : PHuffmanTree;
  original        : AnsiString;

Begin
  handle_in   := File_open(file_in, FILE_MODE_READ);
  buffer      := File_read(handle_in, 4);

  { comprubo que es un archivo DBZ }
  if (pos('DBZ', PChar(ByteBuffer_data(buffer))) <> 0) then
    begin
      ext_size := ByteBuffer_get(buffer, 4);
      ByteBuffer_free(buffer);

      { obtengo la longitud del arbol }
      buffer := File_read(handle_in, 3);
      i               := 1;
      tree_spec_size  := 0;
      while (i < ByteBuffer_length(buffer)) do
        begin
          tree_spec_size := (tree_spec_size shl 8) or ByteBuffer_get(buffer, i);
          inc(i);
        end;

      { obtengo truncamiento del arbol }
      trunk_tree := ByteBuffer_get(buffer, 3);
      ByteBuffer_free(buffer);

      { obtengo longitud del mensaje }
      buffer := File_read(handle_in, 5);
      i := 1;
      msj_size := 0;
      while (i < ByteBuffer_length(buffer)) do
        begin
          msj_size := (msj_size shl 8) or ByteBuffer_get(buffer, i);
          inc(i)
        end;

      { obtengo truncamiento del mensaje }
      trunk_msj := ByteBuffer_get(buffer, 5);
      ByteBuffer_free(buffer);

      { leo la extension }
      buffer := File_read(handle_in, ext_size);
      i := 1;

      if (ByteBuffer_length(buffer) > 0) then
        file_out := file_out + '.';

      while (i <= ByteBuffer_length(buffer)) do
        begin
          file_out := file_out + (chr(ByteBuffer_get(buffer, i)));
          inc(i);
        end;
      ByteBuffer_free(buffer);

      { abro el archivo de salida }
      handle_out := File_open(file_out, FILE_MODE_WRITE);

      { leo el arbol }
      buffer := File_read(handle_in, tree_spec_size);
      tree_spec := '';
      aux := '';
      i := 1;
      while (i <= ByteBuffer_length(buffer)) do
        begin
          aux := decToBinStr(ByteBuffer_get(buffer, i));
          completeStrByte(aux);
          tree_spec := tree_spec + aux;
          inc(i);
        end;
      ByteBuffer_free(buffer);

      tree_spec := copy(tree_spec, trunk_tree + 1, length(tree_spec));

      { leo el mensaje }
      buffer := File_read(handle_in, msj_size);
      msj := '';
      aux := '';
      i := 1;
      while (i <= ByteBuffer_length(buffer)) do
        begin
          aux := decToBinStr(ByteBuffer_get(buffer, i));
          completeStrByte(aux);
          msj := msj + aux;
          inc(i);
        end;
      ByteBuffer_free(buffer);

      msj := copy(msj, trunk_msj + 1, length(msj));

      tree := HuffmanTree_newFromStringSpec(tree_spec);
      tree_spec := '';

      original := HuffmanTree_getOriginalMessage(tree, msj);
      msj := '';
      writeFile(handle_out, original);

    end;

  ByteBuffer_free(buffer);
  File_close(handle_out);
  File_close(handle_in);
  ByteBuffer_free(buffer);
End;

////////////////////////////////////////////////////////////////////////////////
Procedure writeFile(var f: PFile; var str: AnsiString);
Var
  buffer  : PByteBuffer;

Begin
  buffer := ByteBuffer_newFromBinaryStr(str);
  File_write(f, buffer, 0);
  ByteBuffer_free(buffer);
End;

END.
